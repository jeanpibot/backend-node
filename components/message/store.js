const db = require('rethinkdb');

const createTable = (conn, tableName) => {
    db.createTable(tableName).run(conn, (err, result) => {
        if (err) throw err;
        console.log(JSON.stringify(result));
    })
}

const addUser = (datos, conn) => {
    db.table('users').insert(datos).run(conn, (err, result) => {
        if (err) throw err;
        console.log(JSON.stringify(result))
    });
}

const getMessage = (conn) => {
    db.table('users').run(conn, (err, cursor) => {
        if (err) throw err;
        cursor.toArray()
    })
}

module.exports = {
    add: addUser,
    list: getMessage,
    crear: createTable,
}