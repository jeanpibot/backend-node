const rethinkdb = require('rethinkdb');
const store = require('./store');

const connection = () => {
    rethinkdb.connect({ host: 'localhost', port: 28015, db: 'test'}, (err, conn) => {
        if (err) throw err;
        store.crear(conn, 'users');
    })
    return conn;
}

module.exports = connection;

