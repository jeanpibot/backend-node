const  express = require('express');
const response = require('../../network/response');
//controller
const controller = require('./controllers');

const router = express.Router();

router.get('/', (req, res) => {
    controller.getMessage()
        .then((messageList) => {
            response.success(req, res, messageList, 200);
        })
        .catch(e => {
            response.error(req, res, 'Unexpected Error', 500, e);
        })
});

router.post('/', (req, res) => {
    controller.addMessage(req.body.user, req.body.message)
            .then(() => {
                response.success(req, res, 'Creado correctamente', 201);
            })
            .catch(e => {
                response.error(req, res, 'Información invalida', 400, e);
            })
});

router.delete('/', (req, res) => {
    res.send(`Mensaje ${req.body.text} añadido correctamente`)
})

module.exports = router;