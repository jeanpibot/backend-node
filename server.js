const express = require('express');
const bodyParser = require('body-parser');

const routes = require('./network/routes');

const app = express();

routes(app);
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));



app.use('/app', express.static('public'));

app.listen(3000, () => {
    console.log('La aplicación está escuchando en http://localhost:3000');
});